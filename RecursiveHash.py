from DirectoryMapper import DirectoryMapper
from HashFile import HashFile
import magic

__author__ = 'leonardo.pepe'


class RecursiveHash(object):
    """

    """
    def __init__(self, root_dir_name):
        self.mapper = DirectoryMapper(root_dir_name)
        self.hash = HashFile()
        self.excluded_files = []

    def hash_all_files(self):
        pass

    def recursive(self):
        for filename in self.mapper.all_files.__iter__():
            print('file: {0}, hash: {1}'.format(filename, self.hash.md5_sum(file_path=filename)))


if __name__ == '__main__':
    md5dir = RecursiveHash(root_dir_name='/tmp')
    md5dir.recursive()
