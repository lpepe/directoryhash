from hashlib import md5
import magic


class HashFile(object):
    """
    # Usage 1: Instantiating obj with file_path
    hasher = HashFile()
    print(hasher.md5_sum(file_path='/tmp/tfile1'))

    # Usage Alternative
    hasher = HashFile()
    print(hasher.md5_sum(file_path='/tmp/tfile1'))
    hasher.md5_sum(file_path='/tmp/bfile1')
    print("md5 hash is: {0}".format(hasher.get_md5_sum()))
    """

    def __init__(self):
        """
            If the user do not pass file_path during initialization time

        :rtype : object
            pre: None
            post: initialize file_path and file_hash. If kwargs do not contain file_path
                the attributes file_path and file_hash will initialize as None
        """
        self.file_path = None
        self.file_hash = None
        self.file_obj = None
        self.file_stat = None
        self.md5_chk = md5()

    def __delete__(self, instance):
        del self.file_obj

        """
            pre: None
            post: sets self.file_path
        """

    def md5_sum(self, file_path=None):
        """


        :type file_path: object
        :rtype : object
        :return:
        """

        """
        def md5_ascii_file():
            self.md5_chk = md5()
            f = open(file_path, 'r')
            self.md5_chk.update(f.read())
            f.close()
            return self.md5_chk.hexdigest()
        """

        def md5_binary_file():
            """
                If self.file_path does not exist, self.set_file_path is called
                to set file_path attribute.

            :rtype : object
                pre: file_path and file_hash must not be None
                post: sets self.file_hash with md5.hexdigest result
            """
            # assure that self.file_path is always set
            if self.file_path is not None:
                self.file_path = file_path
            else:
                # Open file_path as binary and read it in chunks of 8192 bytes
                # 8192 is the default file system's block size of a major number of OS.
                try:
                    # Move mime check to DirectoryMapper
                    # must be included the mime field on dir dict
                    if magic.from_file(file_path, mime=True) != 'inode/socket':
                        with open(file_path, 'rb') as f:
                            for chunk in iter(lambda: f.read(8192), b''):
                                self.md5_chk.update(chunk)

                        return self.md5_chk.hexdigest()
                except IOError as e:
                    raise Exception("Could not open {0} file\n""I/O Error {1}: {2}".format(
                        file_path,
                        e.errno,
                        e.strerror
                    ))

        self.file_hash = md5_binary_file()
        return self.file_hash

    def get_md5_sum(self):
        """
            pre: file_hash and file_path must exist
            pos: return file_hash contain md5.hexdigest
        """
        try:
            return self.file_hash
        except NameError:
            raise Exception('md5 hash not present.\nexecute md5_sum() meth first')


if __name__ == '__main__':
    # Usage 1: Instantiating obj with file_path
    hash = HashFile()
    print(hash.md5_sum(file_path='/tmp/tfile1'))

    # Usage Alternative
    hash = HashFile()
    print(hash.md5_sum(file_path='/tmp/tfile1'))
    hash.md5_sum(file_path='/tmp/bfile1')
    print("md5 hash is: {0}".format(hash.get_md5_sum()))
